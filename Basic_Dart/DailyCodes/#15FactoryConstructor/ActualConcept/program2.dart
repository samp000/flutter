	
	/*

		- Factory constructor name and other constructor names should not conflict.
		- Factory constructor always returns object of current class or child classes of current class
		- this keyword is not allowd in factory constructor cause it is not a part of class 

	*/

	class Demo {

		Demo._private() {

			print("In private constructor");
		}
		
		factory Demo() {
			
			print("In factory constructor");
			return Demo._private();			// same as -> return new Demo._private();
		}
	}


