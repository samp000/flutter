
	class Demo {

		static Demo obj = new Demo();

		// this drawback is removed by factory constructor
		
		Demo Demo() {
			
			print("In constructor");
			return obj;
		}
	}

	void main() {

		Demo obj = new Demo();
	}


