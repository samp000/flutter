
	class Backend {
	
		String? framework;

		Backend._code(String lang) {

			if(lang == "JS")
				this.framework = "NodeJs";
			else if(lang == "Java")
				this.framework = "SpringBoot";
			else
				this.framework = "NodeJs/SpringBoot";
		}

		factory Backend(String lang) {
			
			return Backend._code(lang);
		}
	}
