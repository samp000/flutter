
	import 'program4.dart';

	void main() {
	
		Developer obj1 = new Developer("Backend");		//we expect error in this line cause Developer is interface but this is call to factor const. and it									    // factory is not a part of class so we can create object behalf of factory it always return object so 									// it works.
		obj1.devLang();
		
		Developer obj2 = new Developer("Frontend");
		obj2.devLang();
		
		Developer obj3 = new Developer("testing");
		obj3.devLang();
	}
