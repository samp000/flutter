
	//null safety}

	void main() {
	
		//nullable in datatype 
		int? age = null;		
		//nullable String datatype	
		String? name ="sandy";

		print(age);
		print(name);

		age=21;
		name=null;
		
		print(age);
		print(name);
		
	}
