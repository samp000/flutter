
	//named arguments

	void fun(String team,{String? name,int? jerNo}) {

		print(team);
		print(name);
		print(jerNo);
	}

	void main() {

		//fun("India","Virat",18); error
	
		fun("India",name:"Virat");
		fun("India",jerNo:18);
		fun("India",name:"Virat",jerNo:18);
		
	}
