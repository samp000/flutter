
	class Demo {

		void fun() {
		
			print("In fun");
		}
	}

	void main() {

		Demo? obj = null;
		obj.fun();


		//here we cant get NullPointerException , cause in dart null safety is handled at compile time in dart
	}
