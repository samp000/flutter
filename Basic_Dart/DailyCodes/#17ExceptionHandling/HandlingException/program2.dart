
	//ON --> it is used to catch a specific exception

	import 'dart:io';

	void main() {

		print("Start main");
		
		try{
			int x = int.parse(stdin.readLineSync()!);
			print(x);

		}on FormatException {
	
			print("Exception Handled");
			
		}catch(ex) {

			print(ex);
		}
		print("End main");
	}
