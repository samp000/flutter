

	import 'dart:io';

	void main() {

		print("Start main");
		
		try{
			int x = int.parse(stdin.readLineSync()!);
			print(x);

		}catch(ex) {					//catching directly in parent Exception class and then child classes is erroneous in java but works in dart

			print(ex);
		
		}on IntegerDivisionByZeroException {

			print("divide by zero");

		}on FormatException {
	
			print("Format Exception Handled");
			
		}catch(ex) {

			print(ex);
		}

		print("End main");
	}
