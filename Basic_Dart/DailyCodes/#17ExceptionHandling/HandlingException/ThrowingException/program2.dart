	//user defined exception	

	import "dart:io";

	class NoProfitException {

		String str = "";
		
		NoProfitException(this.str);

		String toString() {
		
			return str;
		}
	}

	void main() {

		int empCnt = int.parse(stdin.readLineSync()!);
		String name = stdin.readLineSync()!;	
		int profit = int.parse(stdin.readLineSync()!);

		try {

			if(profit < 0)
				throw new NoProfitException("Loss");
		}catch(data) {

			print(data.toString());
		}

		print("$name $empCnt $profit");
	}
