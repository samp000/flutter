
	// overriding

	class Parent {

		void career() {
			print("Engineering");
		}

		void marry(){
			print("Depika Padukone");
		}
	}

	class Child extends Parent {
		
		void marry() {
			print("Disha Patni");
		}
	}

	void main() {
	
		Child obj = new Child();
		obj.career();
		obj.marry();

		Parent obj1 = new Child();
		obj1.marry();
	}
