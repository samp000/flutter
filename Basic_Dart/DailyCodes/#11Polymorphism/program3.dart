
	class Meta {

		int? noOfEmp;
		String? headQ;
		double? rev;

		Meta(this.noOfEmp,this.headQ,this.rev);

		get getEmpCount => noOfEmp;

		void compHead() {
			
			print(headQ);
		}

		get getRev => rev;
	}

	class WhatsApp extends Meta {
	
		int? noOfEmp;
		double? rev;

		WhatsApp(this.noOfEmp,this.rev,int noOfE,String hQ,double r):super(noOfE,hQ,r);

		get getEmpCount => noOfEmp;
		get getRev => rev;

		void compHead() {
		
			print("WhatsApp");
			print(headQ);
		}

		void test() {

		}
	}

	void main() {

		Meta obj = new WhatsApp(100,1500,200,"California",3000);

		obj.compHead();
		print(obj.getEmpCount);
		print(obj.getRev);

		//obj.test();	error
	}
