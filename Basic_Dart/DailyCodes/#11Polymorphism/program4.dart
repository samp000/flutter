
	class Amazon {

		void func() {
			
			print("Selling");
		}

		void server() {
		
			print("AWS");
		}
	}

	class PrimeVideo extends Amazon  {

		void func() {
			print("Video Streaming");
		}
	}

	void main(){

		Amazon obj = new PrimeVideo();

		obj.server();
		obj.func();
	}
