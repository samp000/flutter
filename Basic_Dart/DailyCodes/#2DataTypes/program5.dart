main() {

	var x = 10;	//once a "var variable" assigned to a specific value.. then it can only store only data of that specific type

	print(x);

	x = 20.5; 	//this gives error
	
	print(x);
	
}
