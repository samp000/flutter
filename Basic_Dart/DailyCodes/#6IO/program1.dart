
//	import 'dart:core';		comes bydefault
	import 'dart:io';
	
	void main() {

		print(stdin.runtimeType);		//stdin is object
		
		stdout.write("Enter name:");
		String? name = stdin.readLineSync();

		//bydefault return type of readLineSync() is String?		

		stdout.write("Enter age:");
		int? age = int.parse(stdin.readLineSync()!);

		/*	here int.parse() takes String as para	by readLineSync() return String?
			so we need to give ! after readLineSync() for converting to String
		*/	

		print("Name:$name");
		print("Age:$age");

	}	
