
	class Parent {

		int x=10;

		Parent() {

			print("In Parent");
			print(this.hashCode);
		}

		void printData() {

			print(x);
		}
	}

	class Child extends Parent {

		int x=20;

		Child() {

			print("In Child");
			print(this.hashCode);
		}

		void dispData() {

			print(x);
		}
	}

	void main() {

		Child obj1 = new Child();

		obj1.printData();
		obj1.dispData();
	}
