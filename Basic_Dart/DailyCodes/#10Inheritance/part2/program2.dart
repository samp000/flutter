

	/*

			Still question in output of this code
		
	*/

	class Parent {

		int x=10;

		Parent() {

			print("In Parent");
		}

		void printData() {

			print(x);
			print("x hash:${this.x.hashCode}");
		}
	}

	class Child extends Parent {

		int x=20;

		Child() {

			print("In Child");
		}

		void dispData() {

			print(x);
			print("x hash:${this.x.hashCode}");
		}
	}

	void main() {

		Child obj1 = new Child();
		Parent obj2 = new Parent();

		obj1.printData();
		obj1.dispData();
		
		obj2.printData();
		
		obj1.printData();
		obj1.dispData();
	}
