
	class Parent {

		Parent() {

			print("Parent constructor");
		}
	}

	class Child extends Parent {

		Child() {

			super();			//error: super() is cot callable untill call()
			print("Child constructor");
		}
	}

	void main() {

		Child obj = new Child();
		
	}
