
	class Parent {

		Parent() {

			print("Parent constructor");
		}

		call() {				//call() dont hver returntype	
			print("In call method");		
		}
	}

	class Child extends Parent {

		Child() {

			super();				//calls to call()	
			print("Child constructor");
		}
	}

	void main() {

		Child obj = new Child();
		obj();		//calls to call();
		
	}
