
	class Parent {

		Parent() {

			print("Parent constructor");
		}
	}

	class Child extends Parent {

		Child() {

			print("Child constructor");
		}
	}

	void main() {

		Child obj = new Child();
		obj();		//error: obj() is cot callable untill call()
	}
