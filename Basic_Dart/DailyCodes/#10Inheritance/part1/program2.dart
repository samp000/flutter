
	class Parent {

		int x=10;
		String str1 = "Surname";

		void parentDisp() {
			print("In Parent method");
		}
	}

	class Child extends Parent {
		
		int y=10;
		String str2 = "Name";

		void childDisp() {
			print("In Child method");
		}
	}

	void main() {

		Parent obj = new Parent();
		print(obj.y);		//error as getter	but why ? 
		print(obj.str2);	//error as getter	?
		obj.childMethod();	//understandable error.
	}
