
	class Parent {

		int x=10;
		String str = "info";

		void parentDisp() {
			print(x);
			print(str);	
		}
	}

	class Child extends Parent {
		
		int x=20;
		String str = "data";

		void childDisp() {
		
			print(x);
			print(str);		
		}
	}

	void main() {

		Child obj = new Child();
		print(obj.x);			//20
		print(obj.str);			//data
		
		obj.parentDisp();		// 20 data ??	=> but parent have x=10 and str=info
		
		print(obj.x);			//20
		print(obj.str);			//info
		obj.childDisp();		//20 data
	}

	/*
		By inheritance all things are inherited to child But, constructor of parent is not inherited
	*/
