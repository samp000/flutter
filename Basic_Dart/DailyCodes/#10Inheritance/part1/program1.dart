
	class Parent {

		int x=10;
		String str = "Surname";

		void parentDisp() {
			print("In Parent method");
		}
	}

	class Child extends Parent {
	
		
	}

	void main() {

		Child obj = new Child();
		obj.parentDisp();
		print(obj.x);
		print(obj.str);
	}
