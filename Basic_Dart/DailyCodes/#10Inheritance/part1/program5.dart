
	class Parent {

		int x=10;
		String str = "info";

		void parentDisp() {
			print(x);
			print(str);	
		}
	}

	class Child extends Parent {
		
		int x=20;
		String str = "data";

		void childDisp() {
		
			print(x);
			print(str);		
		}
	}

	void main() {

				

		Child obj1 = new Child();
		Parent obj2 = new Parent();

		obj1.childDisp();		//20 data	
		obj2.parentDisp();		//10 info
	}

	/*
		By inheritance all things are inherited to child But, constructor of parent is not inherited
	*/
