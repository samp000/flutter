
	class Demo {
	
		final int? x;
		final String? str;

		const Demo(this.x,this.str);
	}

	void main() {
	
		//Demo obj1 = const new Demo(10,"Sandy");	error
		
		Demo obj1 = const Demo(10,"Sandy");	
		Demo obj2 = const Demo(10,"Sandy");	

		print(obj1.hashCode);
		print(obj2.hashCode);
	}
