
	//private variable in dart

	class Player {
	
		int? _jerNo;
		String? _plName;

		//scope of private variable is limited to same file

		Player(this._jerNo,this._plName);

		void playerInfo() {
			
			print(_jerNo);
			print(_plName);
		}
	}

