
	//private variable in dart

	class Player {
	
		int? _jerNo;
		String? _plName;

		//scope of private variable is limited to same file

		Player(this._jerNo,this._plName);

		void playerInfo() {
			
			print(_jerNo);
			print(_plName);
		}
	}

	void main() {

		Player obj1 = new Player(7,"MSD");		
		obj1.playerInfo();
		
		obj1._jerNo = 18;
		obj1._plName = "Virat";
		//above works
				

		obj1.playerInfo();
	}
