
	import 'program1a.dart';

	void main() {

		Demo obj = new Demo();

		obj.display();

		obj.setX(10);
		obj.setStr = "Sandesh";
		obj.setSal = 80.5;

		obj.display();
	
/*
		note:	we cannot call setter using brackets '()' if we have declared setter by set keyword
*/
	}
