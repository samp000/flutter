
	class Demo {

		int? _x;
		String? str;
		double? _sal;

		//way 1
		void setX(int x) {
			_x = x;
		}

		//way 2
		set setStr(String name) {
			str = name;
		}

		//way 3
		set setSal (double sal) => _sal = sal;

		void display() {

			print(_x);
			print(str);
			print(_sal);
		}

	}
