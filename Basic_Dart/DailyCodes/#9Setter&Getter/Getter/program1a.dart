
	class Demo {

		int _x;
		String? str;
		double? _sal;

		Demo(this._x,this.str,this._sal);

		//way 1
		int? getX(){
			
			return _x;
		}

		//way 2
		String? get getStr {
			
			return str;
		}
		
		//way 3
		get getSal => _sal;
	}
