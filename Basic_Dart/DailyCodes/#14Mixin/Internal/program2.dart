
	mixin Demo1 {

		void fun1() {
		
			print("In fun1 Demo1");
		}
	}

	mixin Demo2 {

		void fun2() {
		
			print("In fun2 Demo2");
		}

		void fun1();		// its not required to give body to this method if not used in child class
	}

	class DemoChild with Demo1,Demo2 {
	
	}

	void main() {

		DemoChild obj = new DemoChild();
		obj.fun1();
	}
