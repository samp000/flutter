
	class Parent {

		void m1() {
			print("In m1-parent");
		}
	}

	mixin Demo1 on Parent{

		void fun() {
		
			print("In fun Demo1");
		}
	}

	/*

		If we want to make multiple classes to use same mixin then, then thw both class should be in same hierarchy, The parent class which "with mixin"that
		class should parent of class which want to use same mixin,
		
		class outside the hierarchy tries to use the same mixin then, this gives the error

	*/


	class Normal extends Parent with Demo1 {
		
		void m1() {
			print("In m1-parent");
		}
	}

	void main() {

		Normal obj = new Normal();
		obj.fun();
	}
