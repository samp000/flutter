
	abstract mixin class Demo1 {

		fun1() {
			print("in fun1 Demo1");
		}
	}

	mixin Demo2 on Demo1 {
		
	}


	class DemoChild extends Demo1 with Demo2{

	}

	void main() {

		DemoChild obj = new DemoChild();
		obj.fun1();
	}
