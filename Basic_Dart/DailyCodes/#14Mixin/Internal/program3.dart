
	mixin Demo1 {

		void fun1() {
		
			print("In fun1 Demo1");
		}
	}

	/*
		
		mixin Demo2 extends Object on Demo1		this line is same as below

	*/

	mixin Demo2 on Demo1{		

		void fun2() {
		
			print("In fun2 Demo2");
		}
	}

	class DemoChild with Demo2 {
	
	}

	void main() {

		DemoChild obj = new DemoChild();
		obj.fun1();
	}
