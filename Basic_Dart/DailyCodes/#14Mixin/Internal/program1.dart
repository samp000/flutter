
	mixin Demo1 {

		Demo1() {

			print("In constructor");	// error: mixin cant have constructor
		}

		void fun1(){
			print("In fun1");
		}

		void fun2();
	}

	void main() {

		Demo1 obj1 = new Demo1();	// we cannot instantiate Demo1 as it is mixin
	}

	// mixin is abstract class type that is proved by 2nd error of this code
