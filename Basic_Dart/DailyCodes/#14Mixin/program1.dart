
// abstract class  vs interface
//due to interface if same method we want then need to override again and again
	abstract class IFC {
		
		void material(){
			print("Indian material");
		}
		
		void taste(){
			print("Indian taste");
		}
	}

	class IndianFC implements IFC {

		void material(){
			print("Indian material");
		}
		
		void taste(){
			print("Indian taste");
		}
	}

	class EUFC extends IFC {
		
		void taste(){
			print("Europian tase");
		}
	}

	void main() {

		IndianFC obj = new IndianFC();
		obj.material();
		obj.taste();
	}
