
	//multiple inheritance 

	abstract class InterfaceDemo1  {

		void m1() {
			print("in m1-InterfaceDemo1");
		}
	}
	
	abstract class InterfaceDemo2  {

		void m2() {
			print("in m2-InterfaceDemo1");
		}
	}

	class Demo implements InterfaceDemo1,InterfaceDemo2 {

		void m1() {
			print("in m1-Demo");
		}	
		
		void m2() {
			print("in m2-Demo");
		}	
	}

	void main() {

		Demo obj = new Demo();
		obj.m1();
		obj.m2();
	}
