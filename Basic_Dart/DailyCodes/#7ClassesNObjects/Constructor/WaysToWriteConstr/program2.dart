
	//way2 : optional parameter

	class Company {

		int? empCount;
		String? cmpName;

		Company(this.empCount,[this.cmpName = "SCube"]);
	
		void cmpInfo() {

			print(empCount);
			print(cmpName);
		}
	}

	void main() {

		Company obj1 = new Company(1000);		//no-error
		Company obj2 = new Company(800,"AMD");

		obj1.cmpInfo();
		obj2.cmpInfo();
	}
