
	//way4 : named parameter

	class Employee {

		int? empId;
		String? empName;

		//Employee(this.empId,this.empName);		error if named parameters are passed
		
		Employee({this.empId,this.empName});
	
		void empInfo() {

			print(empId);
			print(empName);
		}
	}

	void main() {

		Employee obj1 = new Employee(empName:"Sandy",empId:10);		
		Employee obj2 = new Employee(empId:8,empName:"Marathe");		

		obj1.empInfo();
		obj2.empInfo();
	}
