
	//way3 : default parameter

	class Company {

		int? empCount;
		String? cmpName;

		Company(this.empCount,{this.cmpName = "SCube"});
	
		void cmpInfo() {

			print(empCount);
			print(cmpName);
		}
	}

	void main() {

		Company obj1 = new Company(1000);		//works
		Company obj2 = new Company(800,"AMD");		//error

		obj1.cmpInfo();
		obj2.cmpInfo();
	}
