
	//Constant Constructor

	class Employee {
	
	/*
		Error
	
		int? empId;
		String? empName; 
	*/

		final int? empId;
		final String? empName;

	
		const Employee(this.empId,this.empName) ;			
	}

	void main() {

		Employee obj = new Employee(10,"Sandy");
	}
