

	class Employee {

		int? empId;
		String? empName;	

		Employee() {
			
			print("Default Constructor");
		}
		
		
		Employee.constructor2({this.empId=10,this.empName="XYZ"}){
			
			print("Para Constructor");
		}

		void getInfo() {
			print(empId);
			print(empName);
		}	
	}

	void main() {

		//because of "named constructor" there is no any way of ambiguity in 'default constructor' & 'constructor with default parameters' see below
	
		Employee obj1 = new Employee();
		Employee obj2 = new Employee.constructor2();
	}
