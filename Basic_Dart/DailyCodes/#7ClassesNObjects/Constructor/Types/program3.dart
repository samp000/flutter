
	//Named Constructor : we can write multiple constructors with help of this

	class Employee {

		int? empId;
		String? empName;	

		Employee() {
			
			print("Default Constructor");
		}
		
		//______.any_identifier
		Employee.constructor1(this.empId); 
		
		Employee.constructor2(this.empId,this.empName); 


		void getInfo() {
			print(empId);
			print(empName);
		}	
	}

	void main() {

		Employee obj1 = new Employee();
		obj1.getInfo();

		Employee obj2 = new Employee.constructor1(100);
		obj2.getInfo();
		
		Employee obj3 = new Employee.constructor2(200,"Sandy");
		obj3.getInfo();
		
	}
