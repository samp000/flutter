	
	class Demo {
	
		int x=10;
		static int y = 20;

		void printData() {
	
			print(x);
			print(y);
		}
		
	}

	void main() {
	
		Demo obj1 = new Demo();
		Demo obj2 = new Demo();

		obj1.printData();
		obj2.printData();

		Demo.y = 100;

		print("----------------------");

		obj1.printData();
		obj2.printData();

	}

