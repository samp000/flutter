
	void main() {

		int x=10;
		double y=20;
		num z = 20;

		print(x.runtimeType);
		print(y.runtimeType);
		print(z.runtimeType);

		print("\n");

		print(x is int);
		print(y is! double);
		print(z is int);
		
	}
