
	abstract class Parent {

		void property() {
			
			print("Gold,Cars,Land");
		}

		void career();
		void marry();
	}

	class Child extends Parent {

		void career() {
			
			print("Astriphysics");
		}

		void marry() {
			
			print("NO ONE");
		}
	}

	void main() {
	
		Parent obj = new Child();

		obj.property();
		obj.career();
		obj.marry();
	}
