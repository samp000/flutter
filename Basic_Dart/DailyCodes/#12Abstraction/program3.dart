
	abstract class Developer {
		
		void develop() {
		
			print("We develop softwares");
		}

		void devType();
	}

	class MobileDev extends Developer {
			
		void devType() {
		
			print("Flutter developer");
		}
	}

	class WebDev extends Developer {

		void devType(){
			
			print("React developer");
		}
	}

	void main() {
		
		Developer obj1 = new MobileDev();
		obj1.develop();
		obj1.devType();
		
		Developer obj2 = new WebDev();
		obj2.develop();
		obj2.devType();

/*
	Error:We cannot instantiate Abstract class		

		Developer obj3 = new Developer();
		obj3.develop();
		obj3.devType();
*/
		
	}
