
	import "dart:collection";

	void main() {

		var compData = Queue();

		// Queue() in  ListQueue() internally
	
		compData.add("Microsoft");
		compData.add("Google");
		compData.add("Amazon");

		print(compData);

		print(compData.runtimeType);

		compData.addFirst("Apple");
		compData.addLast("Nvidia");

		print(compData);
	
		compData.removeLast();	
		
		print(compData);
	}
