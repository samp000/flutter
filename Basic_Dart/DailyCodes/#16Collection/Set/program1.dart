
	import "dart:collection";

	//HashSet

	void main() {

		var s1 = new HashSet();
		
		s1.add(10);
		s1.add(20);
		s1.add(30);
		s1.add(40);

		print(s1);
		print(s1.runtimeType);
		
		var s2 = HashSet.from([10,20,30]);
		print(s2);
		print(s2.runtimeType);

		var s3 = {10,20,30};
	}
