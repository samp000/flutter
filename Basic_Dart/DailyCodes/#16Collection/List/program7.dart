
	import "dart:collection";

	dynamic fun(var v) {
	
		return v*10;
	}

	void main() {
	
		var list = [100,200];
		
		print("--------------forEach--------------");
		//forEach
		list.forEach(print);
		
		print("--------------Join--------------");
		//join
		String res = list.join(":");
		print(res);

		print("--------------Map--------------");
		//map
		var l = list.map((val) => fun(val));
		print(l);
		print(l.runtimeType);

		print("--------------Reduce--------------");
	
		//reduce
		
		/*
			reduce compulsory return value, 

			(val,ele) => val+ele
			
			- val and ele both are elements of list on which map is applied,
			- val+ele does addition of elements of list and only resturns one value(based on type of list) not any list
		*/			

		var r = list.reduce((val,ele) => val+ele);	
		print(r);
		print(r.runtimeType);

		var StringList = ["Sandesh","Marathe"];
		var red = StringList.reduce((val,ele) => val+ele);
		print(red);
		print(red.runtimeType);
	}
