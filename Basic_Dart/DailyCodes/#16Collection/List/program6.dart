	
	//Unmodifiable  list	

	import "dart:collection";

	void main() {
		
		var list1 = [10,20,30,40,50];
		
		var list2 = UnmodifiableListView(list1);		

		print(list1);
		print(list2);
	
		list1.addAll([100,200]);	
	
		print(list1);
		print(list2);
		
		list2.add(10);
	}
