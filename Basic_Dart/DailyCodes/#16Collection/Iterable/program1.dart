
	import "dart:collection";

	void main(){

		var pl = ["Rohit","Shubman","Virat","KLRahul","Shreyash","Hardik"];

		print("-----------any()---------------------");
		var retBool = pl.any((ele) => ele.length == 5);
		print(retBool);
		retBool = pl.any((ele) => ele.length <=4 );
		print(retBool);

		print("-----------contains()---------------------");
		retBool = pl.contains("Virat");
		print(retBool);
		

		print("-----------elementAt()---------------------");
		var retEle = pl.elementAt(2);
		print(retEle);
		

		print("-----------every()---------------------");
		retBool = pl.every((ele)=>ele.length >= 5);
		print(retBool);
		retBool = pl.every((ele)=>ele[0] == 'Z');
		print(retBool);
		
		print("-----------firstWhere()---------------------");
		retEle = pl.firstWhere((ele)=>ele[0]=='S');
		print(retEle);
		
		print("-----------lastWhere()---------------------");
		retEle = pl.lastWhere((ele)=>ele[0]=='S');
		print(retEle);
		
		print("-----------fold()---------------------");
		retEle = pl.fold("Data: ",(prev,ele) => prev+ele);
		print(retEle);

		print("-----------followedBy()---------------------");
		var retEle1 = pl.followedBy(["Ravindra","Bumrah"]);
		print(retEle1);

		print("-----------forEach()---------------------");
		var ret = pl.forEach(print);
		//print(ret);		error

		print("-----------join()---------------------");
		print(pl.join(" -> "));
			
		print("-----------map()---------------------");
		retEle1 = pl.map((ele) => ele+"Ind");
		print(retEle1);
		print(pl);
		
		print("-----------reduce()---------------------");
		retEle = pl.reduce((val,ele) => val+ele);
		print(retEle);

		
		print("-----------singleWhere()---------------------");
		print(pl.singleWhere((ele) => ele[0] == 'H'));	//Hardik
		//print(pl.singleWhere((ele) => ele[0] == 'S'));	//exception
	
		print("-----------skip()---------------------");
		retEle1 = pl.skip(3);
		print(retEle1);

		print("-----------skipWhile()---------------------");
		retEle1 = pl.skipWhile((ele) => ele[0] == 'R');
		print(retEle1);
	
		print("-----------take()---------------------");
		retEle1 = pl.take(3);
		print(retEle1);
		
		print("-----------takeWhile()---------------------");
		retEle1 = pl.takeWhile((ele) => ele[0] == 'R');
		print(retEle1);

		print("-----------toList()---------------------");
		print(pl.toList());

		print("-----------toSet()---------------------");
		print(pl.toSet());

		print("------------where()---------------------");
		retEle1 = pl.where((ele)=>ele[0] == 'S');
		print(retEle1);
	}
