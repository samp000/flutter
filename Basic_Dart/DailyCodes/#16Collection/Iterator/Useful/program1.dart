
	class Employee implements Iterator {

		int index = -1;
	
		var empId = []; 
		var empName = []; 

		Employee(String id,String name) {

			this.empId = id.split(",");
			this.empName = name.split(",");

		}

		bool moveNext() {
			
			if(index < empId.length-1) {	
				index = index + 1;
				return true;
			}

			return false;
		}

		get current {
			
			if(index >=0 && index <= empId.length-1)
				return "${empId[index]} : ${empName[index]}";
		}
	
	}

	void main() {
	
		var emp = new Employee("1,2,3,4","Sandy,Ayush,Surja,Omkar");
		
		while(emp.moveNext()) 
			print(emp.current);
	}
