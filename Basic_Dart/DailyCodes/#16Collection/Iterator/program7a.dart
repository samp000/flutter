
	void main() {
  var players = ["Rohit", "Virat", "MSD", "KLRahul"];
  print("Original players list: $players");

  // Get an iterator from the players list
  var itr = players.iterator;

  // Iterate through the list and modify the element if it matches "MSD"
  while (itr.moveNext()) {
    if (itr.current == "MSD") {
      itr.current = "MS Dhoni"; // Modify the element through the iterator
    }
  }

  print("Modified players list: $players");
}

