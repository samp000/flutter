
	abstract class Developer {
	
		int x=10;
			
		Developer() {
		
			print("Dev constructor");
		}

		void develop() {
		
			print("We develop software");
		}

		void devType();
	}


	class MobileDev implements Developer {

		int x=20;	//if not declare we gets error

		MobileDev() {
			
			print("MobileDev constructor");
		}
	
		void develop() {
		
			print("We develop Mobile App");
		}

		void devType() {

			print("Flutter developer");
		}
	}


	void main() {
	
		Developer obj = new MobileDev();
		obj.develop();
		obj.devType();
	}
