
	abstract class Developer {
		
		void develop() {
		
			print("We develop software");
		}

		void devType();
	}


/*
		- class Developer acts as interface
		- it cant inherit anything but just gets method's signature
*/
	class MobileDev implements Developer {
		
	
		void develop() {
		
			print("We develop Mobile App");
		}

		void devType() {

			print("Flutter developer");
		}
	}


// Now developer acts as abstract Class
	class WebDev extends Developer {

		void devType() {
		
			print("React developer");
		}
	}

	void main() {
	
		Developer obj = new MobileDev();
		obj.develop();
		obj.devType();
	}
