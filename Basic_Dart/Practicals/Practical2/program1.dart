/*	
	10   20   30  40  50
	60   70   80  90  100
	101  102  103 104 105
	106  107  108 109 110
	120  130  140 150 160

*/

	import 'dart:io';
	import 'numbers.dart';

	void main() {
	
		int? x = int.parse(stdin.readLineSync()!);
		
		int no=10;
		
		for(int i=0;i<x;i++) {

			for(int j=0;j<x;){
				
				if(isDuck(no)) {
					stdout.write("${no} ");
					j++;
				}

				no++;
			}

			print(" ");
		}
		
	}
