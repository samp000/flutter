/*	

			  2
		       3  5
		    7 11 13
		17 19 23 29
	     31 37 41 43 47
	
*/

	import 'dart:io';
	import 'numbers.dart';

	void main() {
	
		int? x = int.parse(stdin.readLineSync()!);
		
		int no=2;
		
		for(int i=1;i<=x;i++) {
			
			for(int sp=x;sp>i;sp--)
				stdout.write("   ");

			for(int j=1;j<=i;){
				
				if(isPrime(no)) {
					stdout.write("${no} ");
					j++;
				}

				no++;
			}

			print(" ");
		}
		
	}
