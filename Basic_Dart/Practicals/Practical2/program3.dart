/*	
	
	1   2   3   4   5
	6   7   8   9   11
	22  33  44  55  66
	77  88  99  101 111
	121 131 141 151 161	
*/

	import 'dart:io';
	import 'numbers.dart';

	void main() {
	
		int? x = int.parse(stdin.readLineSync()!);
		
		int no=1;
		
		for(int i=0;i<x;i++) {

			for(int j=0;j<x;){
				
				if(isPalindrome(no)) {
					stdout.write("${no} ");
					j++;
				}

				no++;
			}

			print(" ");
		}
		
	}
