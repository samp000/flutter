/*	
	
	4   6   8   9  10
	12  14  15  16 18
	20  21  22  24 25
	26  27  28  30 32
	33  34  35  36 38
		
*/

	import 'dart:io';
	import 'numbers.dart';

	void main() {
	
		int? x = int.parse(stdin.readLineSync()!);
		
		int no=4;
		
		for(int i=0;i<x;i++) {

			for(int j=0;j<x;){
				
				if(!isPrime(no)) {
					stdout.write("${no} ");
					j++;
				}

				no++;
			}

			print(" ");
		}
		
	}
