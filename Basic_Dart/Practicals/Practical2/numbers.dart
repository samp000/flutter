
	bool isDuck(int no) {
		
		while(no>0) {
		
			if(no%10 == 0)
				return true;
			no = no~/10;		
		}

		return false;
	}

	bool isPrime(int no) {
		
		int cnt = 0;

		for(int i=1;i<=no;i++) {
			
			if(no%i==0)
				cnt++;

			if(cnt > 2)
				return false;
		}

		return true;
	}

	bool isPalindrome(int no) {
		
		int tmp = no;
		int sum = 0;
	
		while(no > 0) {
			
			sum = sum*10 + no%10;
			no = no ~/ 10;		
		}

		if(sum == tmp)
			return true;

		return false;
	}
