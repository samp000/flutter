import 'dart:io';

void main(){
  stdout.write("Enter n:");
  int n = int.parse(stdin.readLineSync()!);

  int col=0,space=0;

  for(int i=1;i<=n*2-1;i++) {

    stdout.write("|");

    if(i<=n){
      col = i;
      space = 2*(n-col);
    }else{
      col = 2*n-i;
      space = 2*(n-col);
    }

    for(int j=1;j<=col;j++)
      stdout.write("* ");

    for(int sp=1;sp<=space;sp++)
      stdout.write("  ");

    for(int j=1;j<=col;j++)
        stdout.write("* ");

    print("");
  }
}
