import 'dart:io';

void main(){
  stdout.write("Enter n:");
  int n = int.parse(stdin.readLineSync()!);

  int col;

  for(int i=1;i<=n*2-1;i++) {

      if(i<=n)
        col = i;
      else
        col = 2*n-i;

    for(int j=1;j<=col;j++)
      stdout.write("* ");

    print("");
  }
}