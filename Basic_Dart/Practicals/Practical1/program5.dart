import 'dart:io';

void main(){
  stdout.write("Enter n:");
  int n = int.parse(stdin.readLineSync()!);

  for(int i=1;i<=n;i++) {
    for(int sp=1;sp<=n-i;sp++)
      stdout.write("  ");

    for(int j=1;j<=2*i-1;j++)
      stdout.write("* ");

    print("");
  }
}
