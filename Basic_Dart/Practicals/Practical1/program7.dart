import 'dart:io';

void main(){
  stdout.write("Enter n:");
  int n = int.parse(stdin.readLineSync()!);

  int col=0;

  for(int i=1;i<=n*2;i++) {

      if(i == n) {

           col = 2*i-1;
          for(int j=1;j<=col;j++)
          stdout.write("* ");
          print("");

      }

      if(i<n )
        col = 2*i-1;
      else  {
        col = 4*n- 2 * i - 1;
      }

    for(int j=1;j<=col;j++)
        stdout.write("* ");

    print("");
  }
}
