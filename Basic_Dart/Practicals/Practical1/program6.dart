import 'dart:io';

void main(){
  stdout.write("Enter n:");
  int n = int.parse(stdin.readLineSync()!);

  for(int i=1;i<=n;i++) {

    for(int j=1;j<=n;j++)

      if(i == j || n-j+1 == i)
        stdout.write("X ");
      else
        stdout.write("  ");

    print("");
  }
}
