import 'dart:io';

void main(){
  stdout.write("Enter n:");
  int n = int.parse(stdin.readLineSync()!);

  for(int i=1;i<=n;i++) {

    for(int sp = n-i;sp>=1;sp--) 
      stdout.write("  ");

    for(int j=1;j<=i;j++)
      stdout.write("A ");

    print("");
  }
}