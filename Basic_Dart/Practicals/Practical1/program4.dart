import 'dart:io';

void main(){
  stdout.write("Enter n:");
  int n = int.parse(stdin.readLineSync()!);

  int col,space;

  for(int i=1;i<=n*2-1;i++) {

      if(i<=n) {
        col = i;
        space = n-col;
      }else{
        col = 2*n-i;
        space = n-col;
      }
    for(int sp=1;sp<=space;sp++)
      stdout.write("  ");

    for(int j=1;j<=col;j++)
      stdout.write("* ");

    print("");
  }
}
