import 'dart:io';

void main(){
  stdout.write("Enter n:");
  int n = int.parse(stdin.readLineSync()!);

  for(int i=1;i<=n;i++) {

    stdout.write("|");

    for(int sp = 2*n-2;sp>=i*2-1;sp--) 
      stdout.write("  ");

    for(int j=1;j<=i*2-1;j++)
      stdout.write("* ");

    print("");
  }
}