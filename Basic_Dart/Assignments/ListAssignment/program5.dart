	
	//modify list: square every element

	import "dart:collection";
	import "dart:io";	
		

	void main() {

		print("Enter size of list: ");
		int sz = int.parse(stdin.readLineSync()!);
		var list = List.filled(sz,0);
		
		for(int i=0;i<sz;i++)
			list[i] = int.parse(stdin.readLineSync()!);	
		
		list = list.map((ele)=>ele*ele).toList();	
		print(list);
	}
