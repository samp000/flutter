	
	//Reverse list

	import "dart:collection";
	import "dart:io";	
		

	void main() {

		print("Enter size of list: ");
		int sz = int.parse(stdin.readLineSync()!);
		var list = List.filled(sz,0);
		
		print("Enter elements in list : ");
		for(int i=0;i<sz;i++)
			list[i] = int.parse(stdin.readLineSync()!);	
		
		var res = list.reversed.toList();

		print("Original List: $list");
		print("Reversed List: $res");
	}
