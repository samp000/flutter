	
	//replace value in list

	import "dart:collection";
	import "dart:io";	
		

	void main() {

		print("Enter size of list: ");
		int sz = int.parse(stdin.readLineSync()!);
		var list = List.filled(sz,0);
		
		print("Enter elements in list : ");
		for(int i=0;i<sz;i++)
			list[i] = int.parse(stdin.readLineSync()!);	

		print(list);
	
		print("Enter index: ");
		int indx = int.parse(stdin.readLineSync()!);
		
		print("Enter element: ");
		int ele = int.parse(stdin.readLineSync()!);
	
		
		if(indx < 0 || indx >= sz){
	
			print("invalid index");
		
		}else {

			list[indx] = ele;
			print(list);
		}
		
	}
