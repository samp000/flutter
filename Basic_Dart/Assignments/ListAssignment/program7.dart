	
	//search element in list

	import "dart:collection";
	import "dart:io";	
		

	void main() {

		print("Enter size of list: ");
		int sz = int.parse(stdin.readLineSync()!);
		var list = List.filled(sz,0);
		
		print("Enter elements in list : ");
		for(int i=0;i<sz;i++)
			list[i] = int.parse(stdin.readLineSync()!);	

	
		print("Enter elemnet to search: ");
		int ele = int.parse(stdin.readLineSync()!);

		int indx = list.indexOf(ele);
	
		print(list);
		
		if(indx != -1)
			print("$ele is present in the list at $indx");
		else
			print("$ele is not present in the list");
	}
