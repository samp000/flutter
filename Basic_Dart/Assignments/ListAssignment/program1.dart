
	//return no of even elements in list

	import "dart:collection";
	import "dart:io";	
		
	int isEven(int ele) {
		
		if(ele%2==0)
			return 1;
		return 0;
			
	}

	void main() {

		var list = [];
		int cnt=0;
		
		print("Enter 6 values in list:");
		
		for(int i=0;i<6;i++)
			list.add(int.parse(stdin.readLineSync()!));	
		
		list.forEach((ele) => cnt+=isEven(ele));

		print(list);
		stdout.write("No of Even elements in list are $cnt\n");
	}
