	
	//Add 2 lists

	import "dart:collection";
	import "dart:io";	
		

	void main() {

		print("Enter size of list: ");
		int sz = int.parse(stdin.readLineSync()!);
		var list1 = List.filled(sz,0);
		var list2 = List.filled(sz,0);
		var res = List.filled(sz,0);
		
		print("Enter elements in list1 : ");
		for(int i=0;i<sz;i++)
			list1[i] = int.parse(stdin.readLineSync()!);	
		
		print("Enter elements in list2 : ");
		for(int i=0;i<sz;i++)
			list2[i] = int.parse(stdin.readLineSync()!);	

		for(int i=0;i<sz;i++) {
			
			res[i] = list1[i]+list2[i];
		}	

		print("List 1: $list1");
		print("List 2: $list2");
		print("Addition of 2 lists: $res");
	}
