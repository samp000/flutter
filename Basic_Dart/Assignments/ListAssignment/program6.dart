	
	//find largest element in list

	import "dart:collection";
	import "dart:io";	
	import "dart:math";	
		

	void main() {

		print("Enter size of list: ");
		int sz = int.parse(stdin.readLineSync()!);
		var list = List.filled(sz,0);
		
		for(int i=0;i<sz;i++)
			list[i] = int.parse(stdin.readLineSync()!);	

		int largest = list[0];	

		for(int i=1;i<sz;i++)
			largest = max(list[i],largest);
	
		print(list);
		print("Largest element in list is $largest");
	}
