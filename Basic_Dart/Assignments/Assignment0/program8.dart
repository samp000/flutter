

	void main() {

		var n=15;

		if(n%3==0 && n%5==0)
			print("${n} is divisible by both");
		else if(n%3 == 0)
			print("${n} is divisible by 3 only");
		else if(n%5==0)
			print("${n} is divisible by 5 only");
		else
			print("${n} is not divisible by 3 or 5");
	}
