import "package:flutter/material.dart";

class FlagMod extends StatefulWidget {
  const FlagMod({super.key});

  @override
  State<FlagMod> createState() => _FlagState();
}

class _FlagState extends State<FlagMod> {
  int cnt = 0;

  @override
  Widget build(BuildContext context) {
    Icon icn = const Icon(Icons.add);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Tiranga..."),
      ),
      body: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Colors.orange, Colors.white, Colors.green])),
        alignment: Alignment.center,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            pole,
            Container(
              decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  orange,
                  white,
                  green,
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.deepOrange,
          onPressed: () {
            setState(() {
              if (cnt >= 5) {
                cnt = -1;
              }
              cnt++;
            });
          },
          child: icn),
    );
  }

  Widget get pole {
    return (cnt > 0)
        ? Container(
            width: 30,
            height: 700,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40),
                topRight: Radius.circular(40),
              ),
              color: Colors.grey,
            ),
          )
        : Container();
  }

  Widget get orange {
    return (cnt > 1)
        ? Container(
            width: 400,
            height: 80,
            color: Colors.deepOrange,
          )
        : Container();
  }

  Widget get white {
    return (cnt > 2)
        ? Container(
            width: 400,
            height: 80,
            color: Colors.white,
            child: ashokaChakra,
          )
        : Container();
  }

  Widget get ashokaChakra {
    return (cnt > 3)
        ? Image.network(
            "https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Ashoka_Chakra.svg/1200px-Ashoka_Chakra.svg.png")
        : const SizedBox();
  }

  Widget get green {
    return (cnt > 4)
        ? Container(
            width: 400,
            height: 80,
            color: Colors.green,
          )
        : Container();
  }
}
