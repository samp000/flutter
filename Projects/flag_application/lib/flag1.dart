import "package:flutter/material.dart";

class FlagMod extends StatefulWidget {
  const FlagMod({super.key});

  @override
  State<FlagMod> createState() => _FlagState();
}

class _FlagState extends State<FlagMod> {
  int cnt = 0;

  @override
  Widget build(BuildContext context) {
    Icon icn = const Icon(Icons.add);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Tiranga..."),
      ),
      body: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Colors.orange, Colors.white, Colors.green])),
        alignment: Alignment.center,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (cnt > 0)
                ? Container(
                    width: 30,
                    height: 700,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40)),
                      color: Colors.grey,
                    ),
                  )
                : Container(),
            Container(
              decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  (cnt > 1)
                      ? Container(
                          width: 400,
                          height: 80,
                          color: Colors.deepOrange,
                        )
                      : Container(),
                  (cnt > 2)
                      ? Container(
                          width: 400,
                          height: 80,
                          color: Colors.white,
                          child: Image.network(
                              "https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Ashoka_Chakra.svg/1200px-Ashoka_Chakra.svg.png"),
                        )
                      : const SizedBox(),
                  (cnt > 3)
                      ? Container(
                          width: 400,
                          height: 80,
                          color: Colors.green,
                        )
                      : Container(),
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.deepOrange,
          onPressed: () {
            setState(() {
              if (cnt >= 5) {
                cnt = -1;
              }
              cnt++;
            });
          },
          child: icn),
    );
  }
}
