import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProfileState();
  }
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              Container(
                margin: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(1.0),
                      margin: const EdgeInsets.only(right: 20),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(width: 2.5, color: Colors.grey),
                      ),
                      child: CircleAvatar(
                        radius: 35.0,
                        backgroundImage: NetworkImage(
                            "https://pbs.twimg.com/media/FQ7KxCVXIAMILRB.jpg"),
                      ),
                    ),
                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("4"),
                            SizedBox(
                              width: 20,
                            ),
                            const Text("201"),
                            SizedBox(
                              width: 10,
                            ),
                            const Text("190")
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("posts"),
                            SizedBox(
                              width: 10,
                            ),
                            const Text("followers"),
                            SizedBox(
                              width: 10,
                            ),
                            const Text("following")
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
