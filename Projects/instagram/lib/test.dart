import 'package:flutter/material.dart';
import 'package:instagram/homePage.dart';

class Test extends StatefulWidget {
  @override
  State<Test> createState() => _Test();
}

class _Test extends State<Test> {
  List<String> data = [
    "virat.kohli",
    "https://imagevars.gulfnews.com/2022/09/12/Anushka-Virat1_18333434bb6_original-ratio.jpg",
    "https://media.gettyimages.com/id/1794726292/photo/mumbai-india-virat-kohli-of-india-celebrates-after-scoring-a-century-overtaking-sachin.jpg?s=612x612&w=0&k=20&c=hrgu0NHIY0qh4AVuFJPg3l-wUhmHbMV9n8qb6m5KE-0=",
  ];

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        SinglePostWidget(
            userName: data[0], profilePhoto: data[1], postPhoto: data[2])
      ],
    ));
  }
}
