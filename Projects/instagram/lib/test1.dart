import 'package:flutter/material.dart';
import 'package:instagram/homePage.dart';

class Test1 extends StatefulWidget {
  @override
  State<Test1> createState() => _Test1();
}

class _Test1 extends State<Test1> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
          width: double.minPositive,
          child: Row(
            children: [
              Container(
                width: 200,
                height: 200,
                color: Colors.blue,
              ),
              Container(
                width: 200,
                height: 200,
                color: Colors.red,
              ),
              Container(
                width: 200,
                height: 200,
                color: Colors.green,
              )
            ],
          )),
    );
  }
}


/*


Expanded(
        child: Container(
          color: Colors.red,
          child: Center(
            child: Text(
              'This is the expanded area',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
      
*/