import 'package:flutter/material.dart';
import 'package:instagram/profilePage.dart';
import 'package:instagram/reelsPage.dart';
import 'package:instagram/test.dart';
import 'package:instagram/test1.dart';
import 'searchpage.dart';
import 'homePage.dart';

class Instagram extends StatefulWidget {
  const Instagram({super.key});
  @override
  State<Instagram> createState() => _MainPage();
}

class _MainPage extends State<Instagram> {
  int _curIndex = 0;

  final List<Widget> pages = [
    HomePage(),
    SearchPage(),
    ReelsPage(),
    SearchPage(),
    Profile(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages[_curIndex],
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _curIndex,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          fixedColor: Colors.black,
          unselectedItemColor: const Color.fromARGB(207, 0, 0, 0),
          onTap: (index) => {
                setState(() {
                  _curIndex = index;
                  print(_curIndex);
                })
              },
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: "home"),
            BottomNavigationBarItem(icon: Icon(Icons.search), label: "Search"),
            BottomNavigationBarItem(
                icon: Icon(Icons.add_box_outlined), label: "Search"),
            BottomNavigationBarItem(
                icon: Icon(Icons.favorite_border_outlined), label: "Search"),
            BottomNavigationBarItem(
                icon: Icon(Icons.person_outlined), label: "Search"),
          ]),
    );
  }
}



/*


*/