import 'package:flutter/material.dart';

class Assignment6 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text("Core2web")]),
        actions: [Icon(Icons.qr_code_2_outlined)],
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(children: [
            Container(width: 200, height: 200, color: Colors.deepPurple[100]),
            const SizedBox(
              height: 10,
            ),
            Container(width: 200, height: 200, color: Colors.deepPurple[200]),
            const SizedBox(
              height: 10,
            ),
            Container(width: 200, height: 200, color: Colors.deepPurple[300]),
            const SizedBox(
              height: 10,
            ),
            Container(width: 200, height: 200, color: Colors.deepPurple[400]),
            const SizedBox(
              height: 10,
            ),
            Container(width: 200, height: 200, color: Colors.deepPurple[500]),
            const SizedBox(
              height: 10,
            ),
            Container(width: 200, height: 200, color: Colors.deepPurple[600]),
            const SizedBox(
              height: 10,
            ),
            Container(width: 200, height: 200, color: Colors.deepPurple[700]),
            const SizedBox(
              height: 10,
            ),
            Container(width: 200, height: 200, color: Colors.deepPurple[800]),
            const SizedBox(
              height: 10,
            ),
            Container(width: 200, height: 200, color: Colors.deepPurple[900]),
            const SizedBox(
              height: 10,
            ),
            Container(width: 200, height: 200, color: Colors.deepPurple[1000]),
            const SizedBox(
              height: 10,
            ),
            Container(
                width: 200, height: 200, color: Colors.deepPurpleAccent[1000]),
            const SizedBox(
              height: 10,
            ),
            Container(width: 200, height: 200, color: Colors.deepPurple[1000]),
          ]),
        ),
      ),
    );
  }
}
