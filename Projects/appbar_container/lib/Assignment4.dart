import 'package:flutter/material.dart';

class Assignment4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text("Core2web")]),
        actions: [const Icon(Icons.qr_code_2_outlined)],
      ),
      body: Center(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 100,
            height: 100,
            color: Colors.deepPurple,
          ),
          const SizedBox(
            width: 10,
          ),
          Container(
            width: 100,
            height: 100,
            color: Colors.deepPurpleAccent,
          )
        ],
      )),
    );
  }
}
