import 'package:flutter/material.dart';

class Assignment3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text("Core2web")]),
        actions: [Icon(Icons.qr_code_2_outlined)],
      ),
      body: Center(
          child: Container(
        width: 360,
        height: 200,
        color: Colors.deepPurple,
      )),
    );
  }
}
