import 'package:flutter/material.dart';

class Assignment2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        title: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text("Assignment 2")]),
        actions: [const Icon(Icons.person)],
      ),
    );
  }
}
