import 'package:appbar_container/Assignment1.dart';
import 'package:appbar_container/Assignment2.dart';
import 'package:appbar_container/Assignment3.dart';
import 'package:appbar_container/Assignment4.dart';
import 'package:appbar_container/Assignment5.dart';
import 'package:appbar_container/Assignment6.dart';
import 'package:appbar_container/Assignment7.dart';
import 'package:appbar_container/Assignment8.dart';
import 'package:appbar_container/Assignment9.dart';
import 'package:appbar_container/Assignment10.dart';
import 'package:appbar_container/PaddingAssignment.dart';

import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: PaddingAssignment(),
    );
  }
}
