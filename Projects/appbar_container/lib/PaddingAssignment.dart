import 'package:flutter/material.dart';

class PaddingAssignment extends StatelessWidget {
  const PaddingAssignment({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text("Padding & Margin Assignment ")]),
        actions: [
          const Icon(
            Icons.person_2,
          )
        ],
      ),
      body: Center(
        child: Row(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.only(right: 100),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 5,
                  color: Colors.red,
                ),
              ),
              width: 200,
              height: 200,
              child: Image.asset(
                "/home/sandy/Desktop/Study/Flutter/flutter/Projects/appbar_container/assets/img/iron.png",
                width: double.infinity,
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(20),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 5,
                  color: Colors.red,
                ),
              ),
              width: 200,
              height: 200,
              child: Image.asset(
                "/home/sandy/Desktop/Study/Flutter/flutter/Projects/appbar_container/assets/img/iron.png",
                width: double.infinity,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
