import 'package:flutter/material.dart';

class Assignment8 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text("Assignment 8")]),
        actions: [
          const Icon(
            Icons.person_2,
          )
        ],
      ),
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              width: 5,
              color: Colors.red,
            ),
            gradient: const LinearGradient(
              begin: Alignment.topLeft,
              colors: [
                Colors.red,
                Color.fromARGB(255, 12, 104, 96),
                Color.fromARGB(255, 165, 7, 7)
              ],
            ),
          ),
          width: 300,
          height: 300,
        ),
      ),
    );
  }
}
