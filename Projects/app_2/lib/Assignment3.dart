import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({super.key});

  @override
  State<Assignment3> createState() => _Assignment3State();
}

class _Assignment3State extends State<Assignment3> {
  int? _selectedIndex = 1;

  final List<String> imageList = [
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVnlhlvPboSP1ueZwHMsBi_qwlA6--M9webA&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSC6JZzCfDkkSjMS7_Wx81h8DM63CCcsCgB0A&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7SFS9rXg4Ax15U7b58L-u5k7MqqcBfXWDw4lVBopyQ_TuAq7eBD8kTMU4VNQlTf4Vk6o&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2uJYU1BRCVD9Wj10iHJU3nYV5zrdw2Y8tbQ&usqp=CAU",
  ];

  void showNextImage() {
    setState(() {
      _selectedIndex = (_selectedIndex! + 1) % imageList.length;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Display Images",
        ),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.network(
            imageList[_selectedIndex!],
            width: 300,
            height: 300,
          ),
          const SizedBox(
            height: 20,
          ),
          ElevatedButton(onPressed: showNextImage, child: const Text("Next")),
          const SizedBox(
            height: 20,
          ),
          ElevatedButton(
              onPressed: () {
                setState(() {
                  _selectedIndex = 0;
                });
              },
              child: const Text("Reset"))
        ],
      )),
    );
  }
}
