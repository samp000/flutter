import 'package:flutter/material.dart';

class Assignment2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Assignent"),
        centerTitle: true,
      ),
      body: Center(
        widthFactor: 5,
        heightFactor: 1,
        child: Container(
          width: 200,
          height: 100,
          color: Colors.blue,
        ),
      ),
    );
  }
}
