import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget {
  const Assignment1({super.key});

  @override
  State<Assignment1> createState() => _Assignent1();
}

class _Assignent1 extends State<Assignment1> {
  @override
  final List<int> numbers = [1, 2, 9, 10, 141, 145, 151, 153, 407, 1112];

  int? _cntPalindrome = 0;
  int? _cntArmstrong = 0;
  int? _cntStrong = 0;

  bool isPalindrome(int n) {
    int num = n;
    int sum = 0;

    while (n != 0) {
      sum = sum * 10 + n % 10;
      n = n ~/ 10;
    }
    return num == sum;
  }

  bool isArmstrong(int n) {
    int num = n;
    int sum = 0;

    while (n != 0) {
      sum = sum + (n % 10) * (n % 10) * (n % 10);
      n = n ~/ 10;
    }

    return sum == num;
  }

  int fact(int n) {
    if (n <= 1) return 1;

    return fact(n - 1) * n;
  }

  bool isStrong(int n) {
    int num = n;
    int sum = 0;

    while (n != 0) {
      sum = sum + fact(n % 10);
      n = n ~/ 10;
    }

    return num == sum;
  }

  void getArmstrong() {
    _cntArmstrong = 0;

    for (int i = 0; i < numbers.length; i++) {
      if (isArmstrong(numbers[i])) _cntArmstrong = _cntArmstrong! + 1;
    }
  }

  void getPalindrome() {
    _cntPalindrome = 0;

    for (int i = 0; i < numbers.length; i++) {
      if (isPalindrome(numbers[i])) {
        _cntPalindrome = _cntPalindrome! + 1;
      }
    }
  }

  void getStrong() {
    _cntStrong = 0;

    for (int i = 0; i < numbers.length; i++) {
      if (isStrong(numbers[i])) {
        _cntStrong = _cntStrong! + 1;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Numbers...'),
        ),
        body: Center(
          child: Container(
              height: 500,
              width: 400,
              decoration: BoxDecoration(
                  border: Border.all(
                      color: const Color.fromARGB(255, 216, 78, 78), width: 3),
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Data: $numbers",
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                          onPressed: () {
                            setState(() {
                              getPalindrome();
                            });
                          },
                          child: const Text("Count of Palindrome")),
                      const SizedBox(
                        height: 20,
                      ),
                      Text("No of palindrome in list: $_cntPalindrome")
                    ],
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                          onPressed: () {
                            setState(() {
                              getArmstrong();
                            });
                          },
                          child: const Text("Count of Armstrong")),
                      const SizedBox(
                        height: 20,
                      ),
                      Text("No of Armstrong in list: $_cntArmstrong")
                    ],
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                          onPressed: () {
                            setState(() {
                              getStrong();
                            });
                          },
                          child: const Text("Count of Strong")),
                      const SizedBox(
                        height: 20,
                      ),
                      Text("No of Strong no. in list: $_cntStrong")
                    ],
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _cntArmstrong = 0;
                          _cntPalindrome = 0;
                          _cntStrong = 0;
                        });
                      },
                      child: const Text("Reset"))
                ],
              )),
        ));
  }
}
