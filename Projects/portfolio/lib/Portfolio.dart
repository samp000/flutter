import 'package:flutter/material.dart';

class Portfolio extends StatefulWidget {
  const Portfolio({super.key});

  @override
  State<Portfolio> createState() {
    return _PortfolioState();
  }
}

class _PortfolioState extends State<Portfolio> {
  int cnt = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Portfolio"),
        backgroundColor: Colors.green,
      ),
      body: Container(
        padding: EdgeInsets.all(15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            userInfo,
            collegeInfo,
            companyInfo,
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            if (cnt > 2) cnt = -1;

            cnt++;
          });
        },
        backgroundColor: Colors.green,
        child: const Text("Next"),
      ),
    );
  }

  Widget get userInfo {
    return (cnt > 0)
        ? Column(
            children: [
              const Align(
                alignment: Alignment.centerLeft,
                child: Row(
                  children: [
                    Text(
                      "Name :",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      "Sandesh Dattatray Marathe",
                      style:
                          TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Image.asset(
                "assets/profile.jpeg",
                width: 180,
              ),
            ],
          )
        : Container();
  }

  Widget get collegeInfo {
    return (cnt > 1)
        ? Column(
            children: [
              const Align(
                alignment: Alignment.centerLeft,
                child: Row(
                  children: [
                    Text(
                      "College:",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      "Pune Institute Of Coputer Technology",
                      style:
                          TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Image.asset(
                "assets/pic.jpg",
                width: 180,
              ),
            ],
          )
        : Container();
  }

  Widget get companyInfo {
    return (cnt > 2)
        ? Column(
            children: [
              const Align(
                alignment: Alignment.centerLeft,
                child: Row(
                  children: [
                    Text(
                      "Dream Company :",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      "Nvidia ",
                      style:
                          TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Image.asset(
                "assets/nvidia.png",
                width: 180,
              ),
            ],
          )
        : Container();
  }
}
