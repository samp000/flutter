import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Assignment5 extends StatefulWidget {
  const Assignment5({super.key});

  @override
  State<StatefulWidget> createState() {
    return _Assignment1State();
  }
}

class _Assignment1State extends State {
  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("Assignment 5"), backgroundColor: Colors.blue),
      body: Center(
        child: Container(
          width: 300,
          child: Form(
            key: _globalKey,
            child: Column(
              children: [
                const SizedBox(
                  height: 100,
                ),
                TextFormField(
                  controller: _controller,
                  validator: (value) {
                    if (value == null || value == "") {
                      return "Enter Username";
                    } else if (value.length < 7 || value.length > 20) {
                      return "Invalid Username";
                    }
                  },
                  decoration: const InputDecoration(
                    hintText: "Enter Username",
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 2,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                TextFormField(
                  validator: (value) {
                    String res = "";

                    if (value == null || value == "") {
                      return "Enter Password";
                    }

                    if (value.length < 7 || value.length > 20) {
                      res += "Password length should be 7-20";
                    }

                    if (!value.contains(RegExp('[0-9]'))) {
                      res += "\nMust contain digit";
                    }

                    if (!value.contains(RegExp('[^a-zA-Z)-9]'))) {
                      res += "\nMust contain special character";
                    }
                    return res;
                  },
                  decoration: const InputDecoration(
                    hintText: "Enter Password",
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 2,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                ElevatedButton(
                    onPressed: () {
                      _controller.text = _controller.text.trim();
                      _globalKey.currentState!.validate();
                    },
                    child: const Text("Submit")),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
