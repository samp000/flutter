import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Assignment2 extends StatefulWidget {
  const Assignment2({super.key});

  @override
  State<StatefulWidget> createState() {
    return _Assignment1State();
  }
}

class _Assignment1State extends State {
  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("Assignment 2"), backgroundColor: Colors.blue),
      body: Center(
        child: Container(
          width: 300,
          child: Form(
            key: _globalKey,
            child: Column(
              children: [
                const SizedBox(
                  height: 100,
                ),
                TextFormField(
                  validator: (value) {
                    if (value == null || value == "") {
                      return "Enter Mobile No";
                    } else if (value.length < 10 ||
                        value.length > 10 ||
                        value.contains(RegExp('[A-Z,a-z]'))) {
                      return "Ivalid mobile No";
                    }
                  },
                  decoration: const InputDecoration(
                    hintText: "Enter Mobile No",
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 2,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                ElevatedButton(
                    onPressed: () {
                      _globalKey.currentState!.validate();
                    },
                    child: const Text("Submit"))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
