import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({super.key});

  @override
  State<StatefulWidget> createState() {
    return _Assignment1State();
  }
}

class _Assignment1State extends State {
  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("Assignment 3"), backgroundColor: Colors.blue),
      body: Center(
        child: Container(
          width: 300,
          child: Form(
            key: _globalKey,
            child: Column(
              children: [
                const SizedBox(
                  height: 100,
                ),
                TextFormField(
                  controller: _controller,
                  validator: (value) {
                    if (value == null || value == "") {
                      return "Enter Email";
                    } else if (!value.contains("@gmail.com")) {
                      return "Invalid Email";
                    }
                  },
                  decoration: const InputDecoration(
                    hintText: "Enter Email",
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 2,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                ElevatedButton(
                    onPressed: () {
                      _controller.text = _controller.text.trim();
                      _globalKey.currentState!.validate();
                    },
                    child: const Text("Submit"))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
