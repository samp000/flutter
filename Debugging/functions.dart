
bool isPalindrome(int num) {

  int n = num;
  int tmp = 0;

  while(num!=0) {
    tmp = tmp * 10 + num%10;
    num = num ~/ 10;
  }

  return n == tmp;

}

int getPalindrome(int start,int end) {

  int cnt = 0;

  for(int i=start;i<=end;i++) {
    if(isPalindrome(i))
      cnt++;
  }

  return cnt;
}