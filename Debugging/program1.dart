import "dart:io";
import 'functions.dart';

void main() {
  print("start:");
  int start = int.parse(stdin.readLineSync()!);

  print("end:");
  int end = int.parse(stdin.readLineSync()!);

  int res = getPalindrome(start, end);

  print(res);
}
