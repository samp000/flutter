
	
	void fun1() {

		for(int i=0;i<10;i++)
			print("in fun1");
	}

	void fun2() {

		for(int i=0;i<10;i++)
			print("in fun1-a");
		
		Future.delayed(Duration(seconds:5),()=>print("-------------"));

		for(int i=0;i<10;i++)
			print("in fun1-b");
	}

	void main() {

		print("Start main");
		
		fun1();		
		fun2();		

		print("end main");
		
	}
