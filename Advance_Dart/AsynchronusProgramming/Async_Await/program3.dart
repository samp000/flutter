
	import "dart:io";


	Future<void> main() async {

		print("start");
		print(await getOrderMessage());		
		print("end");
	}
	


	Future<String> getOrder() async {

		await Future.delayed(Duration(seconds:1),()=> print("Halt..."));
		
		print("Enter order:");

		String order = stdin.readLineSync()!;
		
		return Future.delayed(Duration(seconds:2),()=> "$order");
	}	

	Future<String> getOrderMessage() async {

		var order = await getOrder();
		return "Your order is $order";
	}
