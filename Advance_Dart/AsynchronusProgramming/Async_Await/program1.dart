
	//Future returning value

	Future<String> getOrder() {

		return Future.delayed(Duration(seconds:5),()=>"Burger");
	}	

	/*
		- When we use awit then that function must be async
		- When we use async function then that function must be of return type Future<>
	*/	
	Future<String> getOrderMessage() async {

		var order = await getOrder();
		return "Your order is $order";
	}

	/*
		- if main function is async then no need to make returntype of main of Future type
		- but beigng consistant it is good to it..
	*/
	Future<void> main() async {

		print("start");
		print(await getOrderMessage());		
		print("end");
	}
