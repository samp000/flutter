
	import "dart:io";
	
	void main() async {

		Directory dir = new Directory("/Tmp");
		
		try {	
			
			await dir.create();
			print("Directory created...");
		}catch(e) {

			print("Directory Creation failed...${e}");
		}

		print("Program ended...");
	}
