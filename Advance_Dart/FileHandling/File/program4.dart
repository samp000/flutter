
	import "dart:io";

	void main() async {

		File f = new File("C2w.txt");

		//sync
		print(f.lengthSync());

		//async way 1
		final data = await f.length();
		print(data);

		//async way 2
		final value = f.length();

		value.then((val) => print(val));
		
	}
