
	import "dart:io";

	void main() async{

		File f = new File("C2w.txt");

		print(f.absolute);
		print(f.path);

		print(await f.lastAccessed());
		print(await f.lastModified());

		print(await f.length());
		print(await f.exists());

		
		// above all methods are async so, written type is Future<> but gives 
		// but gives proper output due to, "await" 
	}
