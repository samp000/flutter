
	import "dart:io";

	
	Future<void> insertRecord(File f) async {
		
		print("-----------Enter user info-------------");

		String? name;
		int? userId;
		double? salary;

		print("Enter user ID:");
		userId = int.parse(stdin.readLineSync()!);
		
		print("Enter user Name:");
		name = stdin.readLineSync();

		print("Enter salary:");
		salary = double.parse(stdin.readLineSync()!);

		await f.writeAsString("\nID: ${userId}, Name: ${name}, Salary: ${salary}",mode:FileMode.append);
		print("record inserted....");
		print("----------------------------------------");
	}

	Future<void> readData(File f) async {
		
		if(!f.existsSync()) { 
			print("No records found.....");
			print("First insert data....");
		}else {
			
			print("\n-----------------Data in file------------------\n");
			print(await f.readAsString());
		}

		print("----------------------------------------");
	}

	void main() async{
	
		File f = new File("UserInfo.txt");
		
		if(!f.existsSync()) {
			await f.create();
			print("File is Created...");
		}

	
		while(true) {	
			
			print("Select any option:");
			print("1.Insert Record");
			print("2.Read Data");
			print("3.Exit");
			print("-------------------------------------------------");

			int choice = int.parse(stdin.readLineSync()!);
			
			switch(choice) {
			
				case 1:await insertRecord(f);

				case 2:await readData(f);

				case 3:
					return;
			}
		}

	}
